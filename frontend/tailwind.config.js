const colors = require("tailwindcss/colors")

module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        neutral: colors.neutral,
        primary: colors.purple,
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            h2: {
              fontSize: "1.875rem",
            },
            a: {
              color: theme("colors.primary.500"),
            },
          },
        },
      }),
      maxHeight: {
        half: "50vh",
      },
    },
    container: {
      center: true,
      padding: {
        DEFAULT: "1rem",
        xs: "1rem",
        sm: "2rem",
        xl: "5rem",
        "2xl": "6rem",
      },
    },
  },
  plugins: [
    require("@tailwindcss/line-clamp"),
    require("@tailwindcss/typography"),
  ],
}
