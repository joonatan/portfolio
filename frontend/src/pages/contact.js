import React from "react"
import { graphql } from "gatsby"
import { useTranslation } from "react-i18next"

import Layout from "../components/layout"
import Seo from "../components/seo"
import Headings from "../components/headings"
import ContactForm from "../components/contact-form"

const ContactPage = () => {
  const [submitted, setSubmitted] = React.useState(false)

  const { t } = useTranslation()

  return (
    <Layout>
      <Seo seo={{ metaTitle: t("Contact") }} />
      <Headings title={t("Contact")} />
      <main className="mx-4 md:mx-auto">
        {!submitted && <ContactForm onSubmitted={setSubmitted} />}
      </main>
    </Layout>
  )
}

export default ContactPage

export const query = graphql`
  query ($language: String!) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`
