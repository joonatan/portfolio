import React from "react"
import { graphql } from "gatsby"
import { Link, useTranslation } from "gatsby-plugin-react-i18next"

import Layout from "../components/layout"
import ArticlesGrid from "../components/articles-grid"
import Seo from "../components/seo"
import Headings from "../components/headings"
import ReferenceGrid from "../components/reference-grid"

const IndexPage = ({ data }) => {
  const { t } = useTranslation()
  const { allStrapiArticle, allStrapiReference, strapiHome, strapiGlobal } = data

  return (
    <Layout>
      <Seo seo={{ metaTitle: t("title") }} />
      <Headings
        title={strapiGlobal.siteName}
        description={strapiGlobal.siteDescription}
        cover={strapiHome.cover}
      />
      <main>
        <div className="prose mx-auto py-8 dark:prose-invert">
          <div
            className="mx-4"
            dangerouslySetInnerHTML={{
              __html: strapiHome.shortcopy.data.childMarkdownRemark.html,
            }}
          />
        </div>
        <div className="flex w-full justify-between">
          <Link
            className="mx-auto rounded bg-primary-200 py-2 px-8 font-semibold uppercase shadow-lg dark:bg-primary-900"
            to="/contact/"
          >
            {t("contactBtn")}
          </Link>
        </div>
        <h2 className="mt-12 text-center text-3xl font-bold">References</h2>
        <ReferenceGrid references={allStrapiReference.nodes} />
        <h2 className="mt-12 text-center text-3xl font-bold">Posts</h2>
        <ArticlesGrid articles={allStrapiArticle.nodes} />
        <div className="prose mx-auto py-8 dark:prose-invert">
          <div
            className="mx-4"
            dangerouslySetInnerHTML={{
              __html: strapiHome.longcopy.data.childMarkdownRemark.html,
            }}
          />
        </div>
      </main>
    </Layout>
  )
}

export default IndexPage

export const query = graphql`
  query ($language: String!) {
    allStrapiArticle {
      nodes {
        ...ArticleCard
      }
    }
    allStrapiReference(
      filter: { locale: { eq: $language } }
      sort: { order: ASC, fields: order }
    ) {
      nodes {
        ...ReferenceCard
      }
    }
    strapiHome(locale: { eq: $language }) {
      title
      cover {
        alternativeText
        localFile {
          childImageSharp {
            gatsbyImageData(layout: FULL_WIDTH)
          }
        }
      }
      shortcopy {
        data {
          childMarkdownRemark {
            html
          }
        }
      }
      longcopy {
        data {
          childMarkdownRemark {
            html
          }
        }
      }
    }
    strapiGlobal(locale: { eq: $language }) {
      siteName
      siteDescription
    }
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`
