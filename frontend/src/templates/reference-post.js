import React from "react"
import { graphql } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { useTranslation } from "gatsby-plugin-react-i18next"

import Layout from "../components/layout"
import BlocksRenderer from "../components/blocks-renderer"
import Seo from "../components/seo"

const ReferencePage = ({ data }) => {
  const reference = data.strapiReference

  const { t } = useTranslation()

  const seo = {
    metaTitle: reference.title,
    metaDescription: reference.description,
    shareImage: reference.cover,
  }

  return (
    <Layout as="reference">
      <Seo seo={seo} />
      <header>
        <GatsbyImage
          className="max-h-half"
          image={getImage(reference?.cover?.localFile)}
          alt={reference?.cover?.alternativeText}
        />
        <div className="container pt-8">
          <h1 className="mb-4 text-center text-4xl font-bold text-neutral-700 dark:text-neutral-200 md:text-6xl">
            {reference.title}
          </h1>
        </div>
        <div className="grid grid-cols-3">
          <div className="prose col-span-2 mx-4 text-neutral-500 dark:text-neutral-400 md:mx-auto">
            {reference.description && (
              <p className="text-lg md:text-xl">{reference.description}</p>
            )}
            {reference.goal && (
              <p>
                {t("Goal")}: {reference.goal}
              </p>
            )}
            {reference.url && (
              <a
                className="text-blue-500 dark:text-blue-300"
                rel="noreferrer noopener"
                target="_blank"
                href={reference.url}
              >
                {t("Link")}
              </a>
            )}
          </div>
          <div className="prose mr-4 text-right text-neutral-500">
            {reference.tags.map((tag) => (
              <div key={tag.slug}>{tag.name}</div>
            ))}
          </div>
        </div>
      </header>
      <main className="mt-8">
        <BlocksRenderer blocks={reference.blocks || []} />
      </main>
    </Layout>
  )
}

export const pageQuery = graphql`
  query ($language: String!, $slug: String) {
    strapiReference(slug: { eq: $slug }, locale: { eq: $language }) {
      id
      slug
      title
      description
      url
      goal
      tags {
        name
        slug
      }
      blocks {
        ...Blocks
      }
      cover {
        alternativeText
        localFile {
          url
          childImageSharp {
            gatsbyImageData(layout: FULL_WIDTH)
          }
        }
      }
    }
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`

export default ReferencePage
