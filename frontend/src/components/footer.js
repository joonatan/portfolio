import React from "react"

const Footer = ({ email }) => {
  const currentYear = new Date().getFullYear()

  return (
    <footer className="mt-16 flex justify-around bg-neutral-100 py-8 text-neutral-700 dark:bg-neutral-800 dark:text-neutral-300">
      <div>
        <p>Copyright {currentYear}</p>
      </div>
      <div>
        {email && email !== "" && (
          <a className="font-medium" href={`mailto:${email}`}>
            {email}
          </a>
        )}
      </div>
    </footer>
  )
}

export default Footer
