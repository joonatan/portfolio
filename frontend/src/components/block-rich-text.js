import React from "react"

const BlockRichText = ({ data }) => {
  return (
    <div className="prose mx-auto dark:prose-invert">
      <div
        className="mx-4"
        dangerouslySetInnerHTML={{
          __html: data.richTextBody.data.childMarkdownRemark.html,
        }}
      />
    </div>
  )
}

export default BlockRichText
