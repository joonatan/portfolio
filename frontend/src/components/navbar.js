import React from "react"

import { Link, useI18next, useTranslation } from "gatsby-plugin-react-i18next"
import { GatsbyImage, getImage } from "gatsby-plugin-image"

const LanguageSwitcher = () => {
  const { language, languages, originalPath } = useI18next()

  const [selectorOpen, setSelectorOpen] = React.useState(false)

  const toggleSelector = (evt) => {
    evt.preventDefault()
    setSelectorOpen(!selectorOpen)
  }

  return (
    <div className="relative">
      <button
        className="px-2 py-4 uppercase hover:bg-primary-900 hover:text-white dark:hover:bg-primary-600"
        onClick={toggleSelector}
      >
        {language}
        <span className="ml-2">&#9660;</span>
      </button>
      <ul
        className={`${selectorOpen ? "block" : "hidden"}
          absolute top-14 mb-2 bg-primary-600 text-lg`}
      >
        {languages
          .filter((lng) => lng !== language)
          .map((lng) => (
            <li key={lng}>
              <Link
                className="mt-2 p-2 pr-8 uppercase hover:bg-primary-900 hover:text-white dark:hover:bg-primary-600"
                to={originalPath}
                language={lng}
              >
                {lng}
              </Link>
            </li>
          ))}
      </ul>
    </div>
  )
}

const RenderLogo = ({ logo, alt }) => {
  if (!logo || !logo.localFile || !logo.localFile.extension) {
    return alt
  }

  return logo.localFile.extension !== "svg" ? (
    <GatsbyImage image={getImage(logo.localFile)} alt={"Home"} />
  ) : (
    <img width="120" height="60" src={logo.localFile.url} alt={alt} />
  )
}

const Navbar = ({ logo }) => {
  const { t } = useTranslation()

  const links = [
    {
      key: "link-contact",
      to: "/contact",
      text: "Contact",
    },
    {
      key: "link-about",
      to: "/about",
      text: "About",
    },
  ]

  return (
    <header className="sticky top-0 z-10 w-full bg-primary-500 shadow-xl dark:bg-primary-900">
      <nav className="container flex flex-row items-center justify-between">
        <Link to="/" className="py-2 text-xl font-medium">
          <RenderLogo logo={logo} alt="Home" />
        </Link>
        <div className="flex flex-row items-baseline justify-end">
          {links.map((l) => (
            <Link
              key={l.key}
              className="px-2 py-4 font-medium hover:bg-primary-900 hover:text-white dark:hover:bg-primary-600 md:px-4"
              to={l.to}
            >
              {t(l.text)}
            </Link>
          ))}
          <LanguageSwitcher />
        </div>
      </nav>
    </header>
  )
}

export default Navbar
