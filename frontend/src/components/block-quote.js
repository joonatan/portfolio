import React from "react"

const BlockQuote = ({ data }) => {
  return (
    <div className="py-6">
      <blockquote className="container max-w-xl border-l-4 border-neutral-700 py-2 pl-6 text-neutral-700 dark:text-neutral-300">
        <p className="text-4xl font-medium italic md:text-5xl">
          {data.quoteBody}
        </p>
        <cite className="mt-4 block font-bold uppercase not-italic">
          {data.title}
        </cite>
      </blockquote>
    </div>
  )
}

export default BlockQuote
