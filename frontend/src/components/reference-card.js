import React from "react"
import { graphql } from "gatsby"
import { Link } from "gatsby-plugin-react-i18next"
import { GatsbyImage, getImage } from "gatsby-plugin-image"

const ReferenceCard = ({ reference }) => {
  return (
    <Link
      to={`/reference/${reference.slug}`}
      className="relative overflow-hidden rounded-lg bg-neutral-100 shadow-sm transition-shadow hover:shadow-md dark:bg-gray-900"
    >
      <GatsbyImage
        image={getImage(reference.cover?.localFile)}
        alt={reference.cover?.alternativeText}
      />
      <div className="absolute left-0 top-0 rounded-br-lg bg-primary-200 p-4 dark:bg-primary-900">
        {reference.tech}
      </div>
      <div className="flex justify-between px-4 py-4">
        <div className="w-1/4"></div>
        <div>
          <h3 className="text-center text-2xl font-bold text-neutral-700 dark:text-neutral-300">
            {reference.title}
          </h3>
          <p className="mt-2 text-neutral-500 line-clamp-2 dark:text-neutral-400">
            {reference.description}
          </p>
        </div>
        <ul className="w-1/4 text-right text-sm">
          {reference.tags.map((tag) => (
            <li key={tag.name}>{tag.name}</li>
          ))}
        </ul>
      </div>
    </Link>
  )
}

export const query = graphql`
  fragment ReferenceCard on STRAPI_REFERENCE {
    id
    slug
    title
    description
    tech
    tags {
      name
    }
    cover {
      alternativeText
      localFile {
        childImageSharp {
          gatsbyImageData(aspectRatio: 1.77)
        }
      }
    }
  }
`

export default ReferenceCard
