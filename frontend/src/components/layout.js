import React from "react"
import { useStaticQuery, graphql } from "gatsby"

import Footer from "./footer"
import Navbar from "./navbar"

const Layout = ({ children }) => {
  const { strapiGlobal } = useStaticQuery(graphql`
    query {
      strapiGlobal {
        siteName
        siteDescription
        logo {
          localFile {
            extension
            url
            childImageSharp {
              gatsbyImageData(height: 60)
            }
          }
        }
        contactEmail
      }
    }
  `)

  const logo = strapiGlobal.logo
  const contactEmail = strapiGlobal.contactEmail

  return (
    <div className="flex min-h-screen flex-col justify-between bg-neutral-50 text-neutral-900 dark:bg-neutral-800 dark:text-neutral-200">
      <div>
        <Navbar logo={logo} />
        {children}
      </div>
      <Footer email={contactEmail} />
    </div>
  )
}

export default Layout
