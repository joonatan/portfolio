import React from "react"
import ReferenceCard from "../components/reference-card"

const ReferenceGrid = ({ references }) => {
  return (
    <div className="container my-12 grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3">
      {references.map((ref) => (
        <ReferenceCard key={ref.slug} reference={ref} />
      ))}
    </div>
  )
}

export default ReferenceGrid
