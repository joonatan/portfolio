import React from "react"
import { connect } from "react-redux"
import { useTranslation } from "react-i18next"

import { infoMessage } from "../redux/reducers"

const InputField = ({ name, type, value, onChange, validationErrorMsg }) => (
  <div className="group relative z-0 mb-6 w-full text-neutral-500 dark:text-neutral-300 ">
    {type !== "textarea" ? (
      <input
        className="peer block w-full border-0 border-b-2 border-gray-300 bg-transparent py-2 px-0 text-sm outline-none"
        type={type}
        name={name}
        onChange={(evt) => onChange(evt)}
        value={value}
        placeholder=" "
      />
    ) : (
      <textarea
        className="peer block w-full border-0 border-b-2 border-gray-300 bg-transparent py-2 px-0 text-sm outline-none"
        name={name}
        value={value}
        onChange={onChange}
      />
    )}
    <label
      for={name}
      className="absolute top-3 -z-10 -translate-y-6 transform text-sm duration-300 peer-placeholder-shown:translate-y-0 peer-focus:left-0 peer-focus:-translate-y-6 peer-focus:text-blue-600 dark:peer-focus:text-blue-400"
    >
      {name}
    </label>
    <p className="invisible mt-2 text-sm text-pink-600 peer-invalid:visible">
      {validationErrorMsg}
    </p>
  </div>
)

const ContactForm = ({ onSubmitted, dispatch }) => {
  const [email, setEmailImpl] = React.useState("")
  const [name, setNameImpl] = React.useState("")
  const [message, setMessageImpl] = React.useState()

  const { t } = useTranslation()

  const setEmail = (evt) => {
    setEmailImpl(evt.target.value)
  }
  const setName = (evt) => {
    setNameImpl(evt.target.value)
  }
  const setMessage = (evt) => {
    setMessageImpl(evt.target.value)
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    // TODO validate form
    console.log("Contact handle submit")
    console.log("email: ", email)
    console.log("name: ", name)
    console.log("message: ", message)
    onSubmitted(true)
    dispatch(infoMessage("Contact form submitted"))
  }

  return (
    <form
      className="prose mx-auto"
      method="post"
      netlify-honeypot="bot-field"
      data-netlify="true"
      name="contact"
    >
      <input type="hidden" name="bot-field" />
      <input type="hidden" name="form-name" value="contact" />
      <InputField
        name={t("Name")}
        type="text"
        value={name}
        onChange={setName}
      />
      <InputField
        name={t("Email")}
        type="email"
        value={email}
        onChange={setEmail}
        validationErrorMsg="Please provide a valid email address."
      />
      <InputField
        name={t("Message")}
        type="textarea"
        value={message}
        onChange={setMessage}
      />
      <div className="my-4">
        <button
          className="border-none bg-primary-200 py-2 px-8 uppercase text-neutral-800 dark:bg-primary-900 dark:text-neutral-200"
          type="submit"
          onClick={handleSubmit}
        >
          {t("Submit")}
        </button>
      </div>
    </form>
  )
}

export default connect()(ContactForm)
