import React from "react"
import { connect } from "react-redux"

import { GatsbyImage, getImage } from "gatsby-plugin-image"

import Alert from "../components/alert"

const Headings = ({ title, description, cover, alertMsg }) => {
  // if cover image then the header is placed on top of it
  // if not then the header
  return (
    <header className="relative">
      {alertMsg !== "" && <Alert level="info" msg={alertMsg} />}
      <GatsbyImage
        className="max-h-half"
        image={getImage(cover?.localFile)}
        alt={cover?.alternativeText}
      />
      <div className={`${cover ? "absolute top-1/2" : ""} flex w-full`}>
        <div className="mx-auto">
          <div
            className={`${cover ? "bg-primary-200 dark:bg-primary-800" : ""}
              mx-4 p-4 text-center`}
          >
            <h1 className="text-4xl font-bold text-neutral-700 dark:text-neutral-200 sm:text-5xl md:text-6xl">
              {title}
            </h1>
            {description && (
              <p className="mt-4 text-2xl text-neutral-600 dark:text-neutral-300">
                {description}
              </p>
            )}
          </div>
        </div>
      </div>
    </header>
  )
}

export default connect((state) => ({
  alertMsg: state.alert,
}))(Headings)
