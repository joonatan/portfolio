import { createStore } from "redux"
import alertReducer from "./reducers"

// Create a Redux store holding the state of your app.
// Its API is { subscribe, dispatch, getState }.
export default () => createStore(alertReducer)
