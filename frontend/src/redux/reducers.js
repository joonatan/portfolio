const alertReducer = (state = { alert: "" }, action) => {
  switch (action.type) {
    case "alert/set":
      console.log("alert set")
      return { alert: action.data }
    default:
      return state
  }
}

export const infoMessage = (msg) => ({
  type: "alert/set",
  data: msg,
})

export default alertReducer
