import "./src/styles/global.css"
import wrapWithProvider from "./src/redux/reduxWrapper"

export const wrapRootElement = wrapWithProvider
